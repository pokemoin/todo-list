import React from 'react';
import {Link} from 'react-router-dom';

const NotFound = () => {

  return (
      <div className="not-found-page">
        <div className="not-found-msg">
          <h1 className="page-title">
            Page not found
          </h1>
          <div className="home-link">
            <Link to="/">
              return to home
            </Link>
          </div>
        </div>
      </div>
  )
};

export default NotFound;
