import React from 'react';
import {bindActionCreators} from 'redux'
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import * as actions from '../actions'

import Header from '../components/region/Header';
import AsideLeft from '../components/region/AsideLeft';
import Content from '../components/region/Content';
import Loading from '../components/block/Loading';
import ModalSubCategoryAddForm from '../components/form/ModalSubCategoryAddForm';
import ModalCategoryEditForm from '../components/form/ModalCategoryEditForm';

const App = (props) => {

  const {options, filter, todos, actions, ownProps} = props;
  const {isFetching, isEditTask, activeTask, activeCategory, categoryList} = todos;
  const {isShowSubCategoryAddForm, isShowCategoryEditForm} = options;

  return (
      <div className="page home-page">
        <Header options={options}
                filter={filter}
                actions={actions}
                isEditTask={isEditTask}
                activeTask={activeTask}
                activeCategory={activeCategory}
        />
        <main className="main">
          <AsideLeft categoryList={categoryList}
                     actions={actions}
                     ownProps={ownProps}
                     isEditTask={isEditTask}
                     activeTask={activeTask}
          />
          <Content activeCategory={activeCategory}
                   isEditTask={isEditTask}
                   activeTask={activeTask}
                   filter={filter}
                   ownProps={ownProps}
                   actions={actions}
          />
        </main>
        {isShowSubCategoryAddForm ? <ModalSubCategoryAddForm actions={actions}/> : null}
        {isShowCategoryEditForm ? <ModalCategoryEditForm actions={actions}/> : null}
        {isFetching ? <Loading /> : null}
      </div>
  )
};

App.propTypes = {
  actions: PropTypes.object.isRequired,
  options: PropTypes.object.isRequired,
  filter: PropTypes.object.isRequired,
  todos: PropTypes.object.isRequired,
  ownProps: PropTypes.object.isRequired
};

const mapStateToProps = (state, ownProps) => ({
  options: state.options,
  filter: state.filter,
  todos: state.todos,
  ownProps: ownProps
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  actions: bindActionCreators(actions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
