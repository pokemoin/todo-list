import {takeLatest, takeEvery, take, put, call, fork, select, all} from 'redux-saga/effects';
import {delay} from 'redux-saga';
import dotProp from 'dot-prop-immutable';
import uuid from 'uuid';
import fetch from 'isomorphic-fetch';
import * as actions from '../actions';
import {todosSelector} from '../reducers/selectors';

const delayDuration = 500;

const demoCategoryId = uuid.v1();

const demoCategory = {
  children: {},
  id: demoCategoryId,
  isRoot: true,
  isActive: true,
  path: demoCategoryId,
  taskList: [{
    title: 'new task 1',
    id: uuid.v1(),
    done: true,
    description: 'task description'
  }, {
    title: 'new task 2',
    id: uuid.v1(),
    done: false,
    description: 'task description'
  }, {
    title: 'new task 3',
    id: uuid.v1(),
    done: true,
    description: 'task description'
  }],
  title: 'new category'
};

// helper functions
const _createNewCategory = (categoryParams) => {
  const id = uuid.v1();
  return {
    children: {},
    id: id,
    isRoot: categoryParams.isRoot,
    isActive: true,
    path: id,
    taskList: [],
    title: categoryParams.categoryTitle
  }
};

const _createNewSubCategory = (activeCategory, categoryTitle) => {
  const id = uuid.v1();
  return {
    children: {},
    id: id,
    isRoot: false,
    isActive: true,
    path: `${activeCategory.path}.children.${id}`,
    taskList: [],
    title: categoryTitle
  }
};

const _createNewTask = (taskTitle) => {
  const id = uuid.v1();
  return {
    description: '',
    done: false,
    id: id,
    title: taskTitle
  }
};

const _addSubCategory = (categoryList, activeCategory, subCategory) => {
  return dotProp.set(categoryList, activeCategory.path, (c) => {
    c.children = {[subCategory.id]: subCategory, ...c.children};
    return c;
  });
};

const _disableActiveCategory = (categoryList, activeCategory) => {
  return dotProp.set(categoryList, activeCategory.path, (c) => {
    c.isActive = false;
    return c;
  });
};


const _setActiveCategory = (categoryList, category) => {
  return dotProp.set(categoryList, category.path, (c) => {
    c.isActive = true;
    return c;
  });
};

const _updateCategoryTitle = (categoryList, activeCategory, newCategoryTitle) => {
  return dotProp.set(categoryList, activeCategory.path, (c) => {
    c.title = newCategoryTitle;
    return c;
  });
};

const _removeCategory = (categoryList, currentCategory) => {
  return dotProp.delete(categoryList, currentCategory.path);
};

const _addNewTask = (categoryList, activeCategory, newTask) => {
  return dotProp.set(categoryList, activeCategory.path, (c) => {
    c.taskList = [newTask, ...c.taskList];
    return c;
  });
};

const _updateTask = (categoryList, activeCategory, params) => {

  const {task, props} = params;

  return dotProp.set(categoryList, activeCategory.path, (c) => {

    c.taskList = c.taskList.map((t) => {

      if (t.id === task.id) {
        return Object.assign({}, t, props);
      }

      return t;
    });

    return c;
  });
};

const _removeTaskActiveCategory = (categoryList, activeCategory, activeTask) => {
  return dotProp.set(categoryList, activeCategory.path, (c) => {
    c.taskList = c.taskList.filter((t) => {
      return t.id !== activeTask.id;
    });
    return c;
  });
};

// sagas functions
export function* startCreateDemoTodo() {
  yield put(actions.request());
  yield call(delay, delayDuration);
  yield put(actions.receiveTodos({
    activeCategory: demoCategory,
    categoryList: {[demoCategory.id]: demoCategory}
  }))
}

export function* startRemoveCategory(action) {

  yield put(actions.request());

  const currentCategory = action.category;
  const {categoryList} = yield select(todosSelector);

  let newCategoryList = yield call(_removeCategory, categoryList, currentCategory);

  yield call(delay, delayDuration);
  yield put(actions.receiveTodos({
    isEditTask: false,
    activeTask: {},
    activeCategory: {},
    categoryList: newCategoryList
  }));
}

export function* startCreateRootCategory(action) {

  yield put(actions.request());

  const categoryParams = action.params;
  const {activeCategory, categoryList} = yield select(todosSelector);
  const newCategory = yield call(_createNewCategory, categoryParams);

  let newCategoryList = categoryList;

  if (activeCategory.hasOwnProperty('path')) {
    newCategoryList = yield call(_disableActiveCategory, categoryList, activeCategory);
  }

  yield call(delay, delayDuration);
  yield put(actions.receiveTodos({
    activeCategory: newCategory,
    categoryList: {[newCategory.id]: newCategory, ...newCategoryList}
  }))
}

export function* startCreateSubCategory(action) {

  yield put(actions.request());

  const categoryTitle = action.categoryTitle;
  const {activeCategory, categoryList} = yield select(todosSelector);
  const newSubCategory = yield call(_createNewSubCategory, activeCategory, categoryTitle);

  let newCategoryList = categoryList;

  if (activeCategory.hasOwnProperty('path')) {
    newCategoryList = yield call(_disableActiveCategory, categoryList, activeCategory);
  }
  newCategoryList = yield call(_addSubCategory, categoryList, activeCategory, newSubCategory);

  yield call(delay, delayDuration);
  yield put(actions.receiveTodos({
    activeCategory: newSubCategory,
    categoryList: newCategoryList
  }));
}

export function* startUpdateActiveCategory(action) {

  const currentCategory = action.category;
  const {activeCategory, categoryList} = yield select(todosSelector);

  let newCategoryList = categoryList;

  if (activeCategory.hasOwnProperty('path')) {
    newCategoryList = yield call(_disableActiveCategory, categoryList, activeCategory);
  }
  newCategoryList = yield call(_setActiveCategory, newCategoryList, currentCategory);

  yield put(actions.receiveTodos({
    activeCategory: currentCategory,
    categoryList: newCategoryList
  }));
}

export function* startMoveEditTaskToCategory(action) {
  yield put(actions.request());

  const category = action.category;
  const {activeCategory, categoryList, activeTask} = yield select(todosSelector);

  let newCategoryList = yield call(_removeTaskActiveCategory, categoryList, activeCategory, activeTask);
  newCategoryList = yield call(_addNewTask, categoryList, category, activeTask);
  newCategoryList = yield call(_disableActiveCategory, categoryList, activeCategory);
  newCategoryList = yield call(_setActiveCategory, newCategoryList, category);

  yield call(delay, delayDuration);
  yield put(actions.receiveTodos({
    categoryList: newCategoryList,
    activeCategory: category
  }));

}

export function* startUpdateCategory(action) {

  yield put(actions.request());

  const newCategoryTitle = action.categoryTitle;
  const {activeCategory, categoryList} = yield select(todosSelector);

  const newCategoryList = yield call(_updateCategoryTitle, categoryList, activeCategory, newCategoryTitle);

  yield call(delay, delayDuration);
  yield put(actions.receiveTodos({
    categoryList: newCategoryList
  }));
}

export function* startCreateNewTask(action) {

  yield put(actions.request());

  const taskTitle = action.taskTitle;
  const {activeCategory, categoryList} = yield select(todosSelector);
  const newTask = yield call(_createNewTask, taskTitle);
  const newCategoryList = yield call(_addNewTask, categoryList, activeCategory, newTask);

  yield call(delay, delayDuration);
  yield put(actions.receiveTodos({
    categoryList: newCategoryList
  }));
}

export function* startUpdateTaskProps(action) {

  yield put(actions.request());

  const params = action.params;
  const {task, props} = params;
  const {activeCategory, categoryList} = yield select(todosSelector);
  const newCategoryList = yield call(_updateTask, categoryList, activeCategory, params);

  yield call(delay, delayDuration);
  yield put(actions.receiveTodos({
    categoryList: newCategoryList,
    activeCategory: {
      ...activeCategory,
      taskList: activeCategory.taskList.map((t) => {

        if (t.id === task.id) {
          return Object.assign({}, t, props);
        }

        return t;
      })
    }
  }));
}

export default function* root() {
  yield takeLatest(actions.CREATE_DEMO_TODO, startCreateDemoTodo);
  yield takeLatest(actions.CREATE_ROOT_CATEGORY, startCreateRootCategory);
  yield takeLatest(actions.SUBMIT_SUB_CATEGORY_MODAL_FORM, startCreateSubCategory);
  yield takeLatest(actions.UPDATE_ACTIVE_CATEGORY, startUpdateActiveCategory);
  yield takeLatest(actions.SUBMIT_EDIT_CATEGORY_MODAL_FORM, startUpdateCategory);
  yield takeLatest(actions.REMOVE_CATEGORY, startRemoveCategory);
  yield takeLatest(actions.MOVE_EDIT_TASK_TO_CATEGORY, startMoveEditTaskToCategory);
  yield takeLatest(actions.CREATE_NEW_TASK, startCreateNewTask);
  yield takeLatest(actions.UPDATE_TASK_PROPS, startUpdateTaskProps);
}
