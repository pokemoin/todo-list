import * as actions from '../actions/'
import uuid from 'uuid';

describe('actions', () => {

  // task actions
  it('should create an action to CREATE_NEW_TASK', () => {

    const taskTitle = 'test task title';
    const expectedAction = {
      type: actions.CREATE_NEW_TASK,
      taskTitle
    };

    expect(actions.createNewTask(taskTitle)).toEqual(expectedAction)
  });

  it('should create an action to UPDATE_TASK_PROPS', () => {
    const params = {
      task: {
        description: 'task description',
        done: false,
        id: uuid.v1(),
        title: 'task title'
      },
      props: {
        description: 'new task description',
        done: true,
        title: 'new task title'
      }
    };
    const expectedAction = {
      type: actions.UPDATE_TASK_PROPS,
      params
    };

    expect(actions.updateTaskProps(params)).toEqual(expectedAction)
  });

  it('should create an action to EDIT_TASK', () => {

    const task = {
      description: 'task description',
      done: false,
      id: uuid.v1(),
      title: 'task title'
    };
    const expectedAction = {
      type: actions.EDIT_TASK,
      task
    };

    expect(actions.editTask(task)).toEqual(expectedAction)
  });

  it('should create an action to CANCEL_EDIT_TASK', () => {

    const expectedAction = {
      type: actions.CANCEL_EDIT_TASK,
    };

    expect(actions.cancelEditTask()).toEqual(expectedAction)
  });

  // async
  it('should create an action to REQUEST', () => {

    const expectedAction = {
      type: actions.REQUEST,
    };

    expect(actions.request()).toEqual(expectedAction)
  });

  it('should create an action to RECEIVE_TODOS', () => {

    const params = {};

    const expectedAction = {
      type: actions.RECEIVE_TODOS,
      params
    };

    expect(actions.receiveTodos(params)).toEqual(expectedAction)
  });

  // demo
  it('should create an action to CREATE_DEMO_TODO', () => {

    const expectedAction = {
      type: actions.CREATE_DEMO_TODO,
    };

    expect(actions.createDemoTodo()).toEqual(expectedAction)
  });

  // category list
  it('should create an action to MOVE_EDIT_TASK_TO_CATEGORY', () => {

    const category = {};

    const expectedAction = {
      type: actions.MOVE_EDIT_TASK_TO_CATEGORY,
      category
    };

    expect(actions.moveEditTaskToCategory(category)).toEqual(expectedAction)
  });

  it('should create an action to CREATE_ROOT_CATEGORY', () => {

    const params = {};

    const expectedAction = {
      type: actions.CREATE_ROOT_CATEGORY,
      params
    };

    expect(actions.createRootCategory(params)).toEqual(expectedAction)
  });

  it('should create an action to CREATE_SUB_CATEGORY', () => {

    const category = {};

    const expectedAction = {
      type: actions.CREATE_SUB_CATEGORY,
      category
    };

    expect(actions.createSubCategory(category)).toEqual(expectedAction)
  });

  it('should create an action to REMOVE_CATEGORY', () => {

    const category = {};

    const expectedAction = {
      type: actions.REMOVE_CATEGORY,
      category
    };

    expect(actions.removeCategory(category)).toEqual(expectedAction)
  });

  it('should create an action to TOGGLE_SHOW_SUB_CATEGORY_MODAL_FORM', () => {

    const status = true;

    const expectedAction = {
      type: actions.TOGGLE_SHOW_SUB_CATEGORY_MODAL_FORM,
      status
    };

    expect(actions.toggleShowSubCategoryForm(status)).toEqual(expectedAction)
  });

  it('should create an action to TOGGLE_SHOW_SUB_CATEGORY_MODAL_FORM', () => {

    const categoryTitle = 'new category title';

    const expectedAction = {
      type: actions.SUBMIT_SUB_CATEGORY_MODAL_FORM,
      categoryTitle
    };

    expect(actions.submitSubCategoryModalForm(categoryTitle)).toEqual(expectedAction)
  });

  it('should create an action to TOGGLE_SHOW_EDIT_CATEGORY_MODAL_FORM', () => {

    const status = true;

    const expectedAction = {
      type: actions.TOGGLE_SHOW_EDIT_CATEGORY_MODAL_FORM,
      status
    };

    expect(actions.toggleShowEditCategoryForm(status)).toEqual(expectedAction)
  });

  it('should create an action to SUBMIT_EDIT_CATEGORY_MODAL_FORM', () => {

    const categoryTitle = 'new category title';

    const expectedAction = {
      type: actions.SUBMIT_EDIT_CATEGORY_MODAL_FORM,
      categoryTitle
    };

    expect(actions.submitEditCategoryModalForm(categoryTitle)).toEqual(expectedAction)
  });

  it('should create an action to UPDATE_ACTIVE_CATEGORY', () => {

    const category = {};

    const expectedAction = {
      type: actions.UPDATE_ACTIVE_CATEGORY,
      category
    };

    expect(actions.updateActiveCategory(category)).toEqual(expectedAction)
  });

  it('should create an action to TOGGLE_FULL_SCREEN', () => {

    const status = true;

    const expectedAction = {
      type: actions.TOGGLE_FULL_SCREEN,
      status
    };

    expect(actions.toggleFullScreen(status)).toEqual(expectedAction)
  });

  it('should create an action to FILTER_SUBMIT', () => {

    const filterQuery = {};

    const expectedAction = {
      type: actions.FILTER_SUBMIT,
      filterQuery
    };

    expect(actions.taskFilterSubmit(filterQuery)).toEqual(expectedAction)
  });

  it('should create an action to FILTER_RESET', () => {

    const expectedAction = {
      type: actions.FILTER_RESET,
    };

    expect(actions.taskFilterReset()).toEqual(expectedAction)
  });

  it('should create an action to FILTER_SHOW_DONE', () => {

    const filterQuery = {};

    const expectedAction = {
      type: actions.FILTER_SHOW_DONE,
      filterQuery
    };

    expect(actions.taskFilterShowDone(filterQuery)).toEqual(expectedAction)
  });
});
