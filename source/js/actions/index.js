export const CREATE_DEMO_TODO = 'CREATE_DEMO_TODO';

// branding actions
export const TOGGLE_FULL_SCREEN = 'TOGGLE_FULL_SCREEN';

// filter actions
export const FILTER_SUBMIT = 'FILTER_SUBMIT';
export const FILTER_RESET = 'FILTER_RESET';
export const FILTER_SHOW_DONE = 'FILTER_SHOW_DONE';

// category list actions
export const MOVE_EDIT_TASK_TO_CATEGORY = 'MOVE_EDIT_TASK_TO_CATEGORY';
export const TOGGLE_SHOW_EDIT_CATEGORY_MODAL_FORM = 'TOGGLE_SHOW_CATEGORY_MODAL_FORM';
export const SUBMIT_EDIT_CATEGORY_MODAL_FORM = 'SUBMIT_EDIT_CATEGORY_MODAL_FORM';
export const TOGGLE_SHOW_SUB_CATEGORY_MODAL_FORM = 'TOGGLE_SHOW_SUB_CATEGORY_MODAL_FORM';
export const SUBMIT_SUB_CATEGORY_MODAL_FORM = 'SUBMIT_SUB_CATEGORY_MODAL_FORM';
export const CREATE_ROOT_CATEGORY = 'CREATE_ROOT_CATEGORY';
export const CREATE_SUB_CATEGORY = 'CREATE_SUB_CATEGORY';
export const REMOVE_CATEGORY = 'REMOVE_CATEGORY';
export const REQUEST = 'REQUEST';
export const RECEIVE_TODOS = 'RECEIVE_TODOS';
export const UPDATE_ACTIVE_CATEGORY = 'UPDATE_ACTIVE_CATEGORY';

// task list actions
export const CREATE_NEW_TASK = 'CREATE_NEW_TASK';
export const UPDATE_TASK_PROPS = 'UPDATE_TASK_PROPS';
export const EDIT_TASK = 'EDIT_TASK';
export const CANCEL_EDIT_TASK = 'CANCEL_EDIT_TASK';

// task
export function createNewTask(taskTitle) {
  return {
    type: CREATE_NEW_TASK,
    taskTitle
  }
}
export function updateTaskProps(params) {
  return {
    type: UPDATE_TASK_PROPS,
    params
  }
}
export function editTask(task) {
  return {
    type: EDIT_TASK,
    task
  }
}
export function cancelEditTask() {
  return {
    type: CANCEL_EDIT_TASK
  }
}

// async
export function request() {
  return {
    type: REQUEST
  }
}

export const receiveTodos = (params) => ({
  type: RECEIVE_TODOS,
  params
});

// demo
export const createDemoTodo = () => ({
  type: CREATE_DEMO_TODO
});

// category list
export const moveEditTaskToCategory = (category) => ({
  type: MOVE_EDIT_TASK_TO_CATEGORY,
  category
});
export const createRootCategory = (params) => ({
  type: CREATE_ROOT_CATEGORY,
  params
});

export const createSubCategory = (category) => ({
  type: CREATE_SUB_CATEGORY,
  category
});

export const removeCategory = (category) => ({
  type: REMOVE_CATEGORY,
  category
});

// modal form
export const toggleShowSubCategoryForm = (status) => ({
  type: TOGGLE_SHOW_SUB_CATEGORY_MODAL_FORM,
  status
});

export const submitSubCategoryModalForm = (categoryTitle) => ({
  type: SUBMIT_SUB_CATEGORY_MODAL_FORM,
  categoryTitle
});

export const toggleShowEditCategoryForm = (status) => ({
  type: TOGGLE_SHOW_EDIT_CATEGORY_MODAL_FORM,
  status
});

export const submitEditCategoryModalForm = (categoryTitle) => ({
  type: SUBMIT_EDIT_CATEGORY_MODAL_FORM,
  categoryTitle
});

// change active category
export const updateActiveCategory = (category) => ({
  type: UPDATE_ACTIVE_CATEGORY,
  category
});

// branding
export const toggleFullScreen = (status) => ({
  type: TOGGLE_FULL_SCREEN,
  status
});

// task filter
export const taskFilterSubmit = (filterQuery) => ({
  type: FILTER_SUBMIT,
  filterQuery
});

export const taskFilterReset = () => ({
  type: FILTER_RESET
});

export const taskFilterShowDone = (filterQuery) => ({
  type: FILTER_SHOW_DONE,
  filterQuery
});
