import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';

import options from './options';
import filter from './filter';
import todos from './todos';

const rootReducer = combineReducers({
  filter,
  options,
  todos,
  routing
});

export default rootReducer;
