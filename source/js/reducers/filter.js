import {
  FILTER_SUBMIT,
  FILTER_RESET,
  FILTER_SHOW_DONE
} from '../actions';

const initialState = {
  isSearchActive: false,
  basicConfig: {
    showDone: false,
    taskTitle: ''
  },
  filterQuery: {
    showDone: false,
    taskTitle: ''
  }
};

const filter = (state = initialState, action) => {

  switch (action.type) {

    case FILTER_SUBMIT:
      return {
        ...state,
        isSearchActive: true,
        filterQuery: Object.assign({}, state.filterQuery, action.filterQuery)
      };

    case FILTER_RESET:
      return initialState;

    case FILTER_SHOW_DONE:
      return {
        ...state,
        filterQuery: Object.assign({}, state.filterQuery, action.filterQuery)
      };

    default:
      return state;
  }
};

export default filter;
