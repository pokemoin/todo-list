import imgLogo from "../../assets/img/react-hexagon.png";
import {
  TOGGLE_SHOW_EDIT_CATEGORY_MODAL_FORM,
  SUBMIT_EDIT_CATEGORY_MODAL_FORM,
  TOGGLE_FULL_SCREEN,
  TOGGLE_SHOW_SUB_CATEGORY_MODAL_FORM,
  SUBMIT_SUB_CATEGORY_MODAL_FORM
} from '../actions';

const initialState = {
  logo: imgLogo,
  appTitle: 'To-Do list',
  isFullScreen: false,
  isShowSubCategoryAddForm: false,
  isShowCategoryEditForm: false
};

const options = (state = initialState, action) => {

  switch (action.type) {

    case TOGGLE_SHOW_EDIT_CATEGORY_MODAL_FORM:
      return {
        ...state,
        isShowCategoryEditForm: action.status
      };

    case SUBMIT_EDIT_CATEGORY_MODAL_FORM:
      return {
        ...state,
        isShowCategoryEditForm: false
      };

    case TOGGLE_SHOW_SUB_CATEGORY_MODAL_FORM:
      return {
        ...state,
        isShowSubCategoryAddForm: action.status
      };

    case SUBMIT_SUB_CATEGORY_MODAL_FORM:
      return {
        ...state,
        isShowSubCategoryAddForm: false
      };

    case TOGGLE_FULL_SCREEN:
      return {
        ...state,
        isFullScreen: action.status
      };

    default:
      return state;
  }
};

export default options;
