import undoable from 'redux-undo';
import {
  REQUEST,
  RECEIVE_TODOS,
  EDIT_TASK,
  CANCEL_EDIT_TASK
} from '../actions';

const initialState = {
  isFetching: false,
  isEditTask: false,
  activeTask: {},
  activeCategory: {},
  categoryList: {}
};

const todos = (state = initialState, action) => {
  switch (action.type) {

    case REQUEST:
      return { ...state, isFetching: true };

    case RECEIVE_TODOS:
      return {
        ...Object.assign({}, state, action.params),
        isFetching: false
      };

    case EDIT_TASK:
      return {
        ...state,
        isEditTask: true,
        activeTask: action.task
      };

    case CANCEL_EDIT_TASK:
      return {
        ...state,
        isEditTask: false,
        activeTask: {}
      };

    default:
      return state
    }
};

// const undoableTodos = undoable(todos);
// export default undoableTodos;

export default todos;
