import * as actions from '../actions/';
import reducer from './todos'
import uuid from 'uuid'

describe('todos reducer', () => {
  it('should return the initial state', () => {
    expect(
        reducer(undefined, {})
    ).toEqual(
        {
          isFetching: false,
          isEditTask: false,
          activeTask: {},
          activeCategory: {},
          categoryList: {}
        }
    )
  });

  it('should handle REQUEST', () => {
    expect(
        reducer([], {
          type: actions.REQUEST,
          isFetching: true
        })
    ).toEqual(
        {
          isFetching: true
        }
    )
  });

  it('should handle RECEIVE_TODOS', () => {
    expect(
        reducer([], {
          type: actions.RECEIVE_TODOS,
          isFetching: false
        })
    ).toEqual(
        {
          isFetching: false
        }
    )
  });

  it('should handle EDIT_TASK', () => {

    const task = {
          description: 'task description',
          done: false,
          id: uuid.v1(),
          title: 'task title'
        };

    expect(
        reducer({}, {
          type: actions.EDIT_TASK,
          task: task
        })
    ).toEqual(
        {
          isEditTask: true,
          activeTask: task
        }
    )
  });

  it('should handle CANCEL_EDIT_TASK', () => {
    expect(
        reducer([], {
          type: actions.CANCEL_EDIT_TASK,
          isEditTask: false,
          activeTask: {}
        })
    ).toEqual(
        {
          isEditTask: false,
          activeTask: {}
        }
    )
  });
});
