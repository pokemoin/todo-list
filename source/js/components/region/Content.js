import React from 'react';
import PropTypes from 'prop-types';

import TaskListBlock from '../block/TaskListBlock';
import EmptyCategoryListMsg from '../block/EmptyCategoryListMsg';
import TaskEditForm from '../form/TaskEditForm';

const Content = (props) => {

  const {activeCategory: {taskList}, isEditTask, activeTask, filter, actions, ownProps} = props;

  return (
      <section className="content">
        { isEditTask && <TaskEditForm activeTask={activeTask}
                                      actions={actions}
                                      ownProps={ownProps}/>
        }
        {!isEditTask && taskList !== undefined && <TaskListBlock taskList={taskList}
                                                                 filter={filter}
                                                                 ownProps={ownProps}
                                                                 actions={actions}/>
        }
        {!isEditTask && taskList === undefined && <EmptyCategoryListMsg actions={actions} />}
      </section>
  )
};

Content.propTypes = {
  actions: PropTypes.object.isRequired,
  activeCategory: PropTypes.object.isRequired,
  isEditTask: PropTypes.bool.isRequired,
  activeTask: PropTypes.object.isRequired,
  filter: PropTypes.object.isRequired,
  ownProps: PropTypes.object.isRequired
};

export default Content;
