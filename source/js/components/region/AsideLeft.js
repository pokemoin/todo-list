import React from 'react';
import PropTypes from 'prop-types';

import CategoryAddForm from '../form/CategoryAddForm';
import CategoryList from '../list/CategoryList';

const AsideLeft = (props) => {

  const {isEditTask, categoryList, actions} = props;

  return (
      <aside className="aside-left">
        <div className="container">
          {!isEditTask && <CategoryAddForm actions={actions}/>}
          { Object.keys(categoryList).length > 0 &&
            <CategoryList {...props}
                          listClassName=''
            />
          }
        </div>
      </aside>
  )
};

AsideLeft.propTypes = {
  actions: PropTypes.object.isRequired,
  categoryList: PropTypes.object.isRequired,
  isEditTask: PropTypes.bool.isRequired,
  activeTask: PropTypes.object.isRequired,
  ownProps: PropTypes.object.isRequired
};

export default AsideLeft;
