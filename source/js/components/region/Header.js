import React from 'react';
import PropTypes from 'prop-types';

import Branding from '../block/Branding';
import TaskFilterForm from '../form/TaskFilterForm';
import ProgressBar from '../block/ProgressBar';

const Header = (props) => {

  const {isEditTask, activeCategory, activeTask} = props;

  return (
      <header className="header">
        { isEditTask && <div className="container"><h3 className="task-title">{activeTask.title}</h3></div>}
        { !isEditTask &&
          <div className="container">
            <Branding {...props}/>
            <TaskFilterForm {...props}/>
          </div>
        }
        { !isEditTask && <ProgressBar activeCategory={activeCategory} /> }
      </header>
  )
};

Header.propTypes = {
  actions: PropTypes.object.isRequired,
  options: PropTypes.object.isRequired,
  filter: PropTypes.object.isRequired,
  activeTask: PropTypes.object.isRequired,
  activeCategory: PropTypes.object.isRequired,
  isEditTask: PropTypes.bool.isRequired
};

export default Header;
