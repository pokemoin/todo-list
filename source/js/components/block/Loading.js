import React from 'react';

const Loading = (props) => {
  return (
      <div className="loading">
        <div className="loader"></div>
        <div className="title">Loading...</div>
      </div>
  )
};

export default Loading;
