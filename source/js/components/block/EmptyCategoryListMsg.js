import React from 'react';
import PropTypes from 'prop-types';

const EmptyCategoryListMsg = ({actions}) => {
  return (
      <div className="empty-category-list-msg-block">
        <h3 className="msgTitle">Category list is empty, please create new category.</h3>
        <div className="msgAction">
          <button onClick={actions.createDemoTodo}>create demo task list</button>
        </div>
      </div>
  )
};

EmptyCategoryListMsg.propTypes = {
  actions: PropTypes.object.isRequired
};

export default EmptyCategoryListMsg;
