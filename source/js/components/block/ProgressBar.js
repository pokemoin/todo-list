import React from 'react';
import PropTypes from 'prop-types';

const ProgressBar = ({activeCategory: {taskList}}) => {

  let progress = 0;
  let numTask = 0;
  let numTaskDone = 0;

  if (taskList !== undefined && taskList.length > 0) {

    numTask = taskList.length;

    numTaskDone = taskList.filter((task) => {
      return task.done;
    }).length;

    progress = Math.abs((numTaskDone / numTask) * 100);
  }

  const style = {width: Math.abs(progress) + '%'};

  return (
      <div className="progress-bar">
        <div className="progress-indicator" style={style}></div>
      </div>
  )
};

ProgressBar.propTypes = {
  activeCategory: PropTypes.object.isRequired,
};

export default ProgressBar;
