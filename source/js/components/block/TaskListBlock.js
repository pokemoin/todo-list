import React from 'react';
import PropTypes from 'prop-types';

import UndoRedo from '../block/UndoRedo';
import TaskAddForm from '../form/TaskAddForm';
import TaskList from '../list/TaskList';

const _filterTaskList = (filter, taskList) => {

  const {filterQuery: {showDone, taskTitle}} = filter;

  if (showDone) {
    taskList = taskList.filter((task) => {
      return task.done;
    });
  }

  if (taskTitle !== '') {
    taskList = taskList.filter((task) => {
      return task.title.indexOf(taskTitle) !== -1;
    });
  }

  return taskList;
};

const TaskListBlock = ({taskList, filter, actions, ownProps}) => {

  const filterTaskList = _filterTaskList(filter, taskList);

  return (
      <div className="task-list-block">
        <div className="task-list-prefix">
          {/*<UndoRedo />*/}
          <TaskAddForm actions={actions}/>
        </div>
        { filterTaskList.length > 0 && <TaskList taskList={filterTaskList}
                                                 ownProps={ownProps}
                                                 actions={actions}/>
        }
      </div>
  )
};

TaskListBlock.propTypes = {
  actions: PropTypes.object.isRequired,
  filter: PropTypes.object.isRequired,
  taskList: PropTypes.array.isRequired,
  ownProps: PropTypes.object.isRequired
};

export default TaskListBlock;
