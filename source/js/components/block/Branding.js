import React from 'react';
import PropTypes from 'prop-types';

export default class HeaderBranding extends React.Component {
  constructor(props) {
    super(props);
    this.onClickHandle = this.onClickHandle.bind(this);
    this.onfullscreenchange = this.onfullscreenchange.bind(this);
  }

  onfullscreenchange () {

    const {actions} = this.props;
    let isFullScreen = false;
    let fullscreenElement =  document.fullscreenElement || document.webkitFullscreenElement || document.mozFullScreenElement || document.msFullscreenElement;

    if (fullscreenElement !== undefined) {
      isFullScreen = true;
    } else {
      isFullScreen = false;
    }

    actions.toggleFullScreen(isFullScreen);
  }

  componentDidMount() {
    document.addEventListener("webkitfullscreenchange", this.onfullscreenchange);
    document.addEventListener("mozfullscreenchange", this.onfullscreenchange);
    document.addEventListener("fullscreenchange", this.onfullscreenchange);
  }

  componentWillUnmount() {
    document.removeEventListener("webkitfullscreenchange", this.onfullscreenchange);
    document.removeEventListener("mozfullscreenchange", this.onfullscreenchange);
    document.removeEventListener("fullscreenchange", this.onfullscreenchange);
  }

  onClickHandle() {

    const {actions} = this.props;
    let isFullScreen = false;

    if (!document.fullscreenElement && !document.mozFullScreenElement && !document.webkitFullscreenElement) {

      isFullScreen = true;

      if (document.documentElement.requestFullscreen) {
        document.documentElement.requestFullscreen();
      } else if (document.documentElement.mozRequestFullScreen) {
        document.documentElement.mozRequestFullScreen();
      } else if (document.documentElement.webkitRequestFullscreen) {
        document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
      }
    } else {

      isFullScreen = false;

      if (document.cancelFullScreen) {
        document.cancelFullScreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.webkitCancelFullScreen) {
        document.webkitCancelFullScreen();
      }
    }

    actions.toggleFullScreen(isFullScreen);
  }

  render() {

    const {options} = this.props;
    const {isFullScreen, logo, appTitle} = options;

    return (
        <div className={`branding${isFullScreen ? ' active' : ''}`}
             onClick={this.onClickHandle}
        >
          <div className="logo">
            <img src={logo} alt="logo"/>
          </div>
          <h1 className="page-title">
            {appTitle}
          </h1>
        </div>
    )
  }
}

HeaderBranding.propTypes = {
  options: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
};
