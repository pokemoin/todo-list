import React from 'react';
import PropTypes from 'prop-types';

import CategoryListItem from '../list-item/CategoryListItem';

const CategoryList = (props) => {

  const {categoryList, listClassName} = props;
  const newListClassName = listClassName !== '' ? ` ${listClassName}` : ``;

  return (
      <div className={`category-list-block ${newListClassName}`}>
        <nav className='category-list'>
          { Object.keys(categoryList).map((id) =>
              <CategoryListItem key={id}
                                {...props}
                                category={categoryList[id]}
              />
          )}
        </nav>
      </div>
  )
};

CategoryList.propTypes = {
  actions: PropTypes.object.isRequired,
  categoryList: PropTypes.object.isRequired,
  listClassName: PropTypes.string.isRequired,
  isEditTask: PropTypes.bool.isRequired,
  activeTask: PropTypes.object.isRequired,
  ownProps: PropTypes.object.isRequired
};


export default CategoryList;
