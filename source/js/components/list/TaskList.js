import React from 'react';
import PropTypes from 'prop-types';
import {Link, NavLink} from 'react-router-dom';

class TaskListItem extends React.Component {
  constructor(props) {
    super(props);
    this.handleShowDone = this.handleShowDone.bind(this);
  }

  handleShowDone(e) {
    const {actions, task} = this.props;
    actions.updateTaskProps({
      task,
      props: {
        done: e.target.checked
      }
    });
  }

  render() {

    const {actions, task, ownProps: {match}} = this.props;
    const {done: isDone, title, description} = task;
    const {url, params: {categoryId, taskId, action}} = match;
    const pathTask = `/${categoryId}/${task.id}`;
    const pathTaskEdit = `${pathTask}/edit`;

    return (
        <li className="task">
          <div className="task-wrap">
            <div className="task-done">
              <input type="checkbox"
                     name="showDone"
                     defaultChecked={isDone}
                     onChange={this.handleShowDone}
              />
            </div>
            <NavLink to={pathTask}
                     activeClassName='active'
                     className="task-title"
            >
              {title}
            </NavLink>
            <div className="task-description">
              {description}
            </div>
            <div className="task-actions">
              <Link to={pathTaskEdit}
                    className="action task-edit"
                    onClick={() => actions.editTask(task)}
              ></Link>
            </div>
          </div>
        </li>
    )
  }
}

TaskListItem.propTypes = {
  actions: PropTypes.object.isRequired,
  task: PropTypes.object,
  ownProps: PropTypes.object.isRequired
};


const TaskList = ({actions, taskList, ownProps}) => {
  return (
      <ul className='task-list'>
        { taskList.map((task) => {
              return (
                  <TaskListItem key={task.id}
                                task={task}
                                ownProps={ownProps}
                                actions={actions}
                  />
              )
            }
        )}
      </ul>
  )
};

TaskList.propTypes = {
  actions: PropTypes.object.isRequired,
  taskList: PropTypes.array.isRequired,
  ownProps: PropTypes.object.isRequired
};


export default TaskList;
