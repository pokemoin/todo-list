import React from 'react';
import PropTypes from 'prop-types';

class TaskFilterForm extends React.Component {
  constructor(props) {

    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleReset = this.handleReset.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();

    const {actions} = this.props;
    const elements = this.filterForm.elements;
    const showDone = elements.showDone.checked;
    const taskTitle = elements.taskTitle.value;

    if (showDone || taskTitle !== '') {
      actions.taskFilterSubmit({showDone, taskTitle});
    } else if (showDone || taskTitle.length > 0) {
      actions.taskFilterReset();
    }
  }

  handleChange() {

    const {actions} = this.props;
    const elements = this.filterForm.elements;
    const showDone = elements.showDone.checked;
    const taskTitle = elements.taskTitle.value;

    if (showDone || taskTitle !== '') {
      actions.taskFilterSubmit({showDone, taskTitle});
    } else {
      actions.taskFilterReset();
    }
  }

  handleReset(e) {
    e.preventDefault();

    const {filter: {basicConfig: {showDone, taskTitle}}, actions} = this.props;
    const elements = e.target.elements;

    elements.showDone.checked = showDone;
    elements.taskTitle.value = taskTitle;

    actions.taskFilterReset();
  }

  render() {

    const {filter: {isSearchActive, basicConfig}} = this.props;
    const {showDone, taskTitle} = basicConfig;

    return (
        <div className="task-filter">
          <form className="filter-form"
                onSubmit={this.handleSubmit}
                onReset={this.handleReset}
                ref={(filterForm) => {this.filterForm = filterForm;}}
          >
            <div className="filter-form__done filter-form__item">
              <label>
                <input type="checkbox"
                       name="showDone"
                       defaultChecked={showDone}
                       onChange={this.handleChange}
                />
                <span>
                  Show done
                </span>
              </label>
            </div>
            <div className="filter-form__task-title filter-form__item">
              <input type="text"
                     name="taskTitle"
                     placeholder="Search"
                     defaultValue={taskTitle}
                     onChange={this.handleChange}
              />
            </div>
            <div className="filter-form__actions filter-form__item">
              <button className={`${!isSearchActive ? 'active' : '' }`}
                      type="submit"
              ></button>
              <button className={`${isSearchActive ? 'active' : '' }`}
                      type="reset"
              ></button>
            </div>
          </form>
        </div>
    )
  }
}
;

TaskFilterForm.propTypes = {
  actions: PropTypes.object.isRequired,
  options: PropTypes.object.isRequired,
  filter: PropTypes.object.isRequired,
  activeTask: PropTypes.object.isRequired,
  activeCategory: PropTypes.object.isRequired,
  isEditTask: PropTypes.bool.isRequired
};

export default TaskFilterForm;
