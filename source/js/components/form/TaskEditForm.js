import React from 'react';
import {Link, Redirect} from 'react-router-dom';
import PropTypes from 'prop-types';

class TaskEditForm extends React.Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {

    const {activeTask, actions} = this.props;
    const form = this.editTaskForm;
    const taskTitle = form.elements.taskTitle.value.trim();
    const isDone = form.elements.showDone.checked;
    const taskDesc = form.elements.taskText.value;

    if (taskTitle !== '') {

      form.classList.remove('error');

      actions.cancelEditTask();
      actions.updateTaskProps({
        task: activeTask,
        props: {
          description: taskDesc,
          done: isDone,
          title: taskTitle
        }
      });

    } else {
      e.preventDefault();
      form.classList.add('error');
    }
  }

  render() {

    const {activeTask: {done: isDone, title, id, description}, ownProps:{match}, actions} = this.props;
    const {url, params: {categoryId, taskId, action}} = match;

    const pathTask = `/${categoryId}`;

    return (
        <div className="edit-task-form-block">
          <form className='edit-task-form'
                ref={(editTaskForm) => {this.editTaskForm = editTaskForm;}}
          >
            <div className='form-item'>
              <input type='text'
                     name='taskTitle'
                     className='task-title__input form-input'
                     placeholder='Enter task title'
                     defaultValue={title}
              />
              <div className="field-error">
                task name is empty
              </div>
            </div>
            <div className="form-item">
              <label className='task-status__label'>
                <input type='checkbox'
                       name='showDone'
                       className='task-status__input form-input'
                       defaultChecked={isDone}
                />
                Done
              </label>
            </div>
            <div className="form-item">
              <textarea className='task-description__textarea'
                        name="taskText"
                        rows="10"
                        placeholder='Enter task description'
                        defaultValue={description}
              >
              </textarea>
            </div>
          </form>
          <div className="action-buttons">
            <Link to={pathTask} onClick={this.handleSubmit}>
              <span className="action-btn submit">
                Save changes
              </span>
            </Link>
            <Link to={pathTask} onClick={actions.cancelEditTask}>
              <div className="action-btn reset">
                Cancel
              </div>
            </Link>
          </div>
        </div>
    )
  }
}

TaskEditForm.propTypes = {
  activeTask: PropTypes.object.isRequired,
  ownProps: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
};

export default TaskEditForm;
