import React from 'react';
import PropTypes from 'prop-types';

class CategoryAddForm extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleSubmit(e) {
    e.preventDefault();

    const {actions} = this.props;
    const form = e.target;
    const elements = form.elements;
    const categoryTitle = elements.categoryTitle.value.trim();
    const isRoot = true;

    if (categoryTitle !== '') {
      form.classList.remove('error');
      elements.categoryTitle.value = '';
      actions.createRootCategory({isRoot, categoryTitle});
    } else {
      form.classList.add('error');
    }
  }
  render() {

    return (
        <div className='category-add-form add-form-block'>
          <form className='form-add' onSubmit={this.handleSubmit}>
            <input type='text'
                   name='categoryTitle'
                   placeholder='Enter category title'
            />
            <button>
              Add
            </button>
          </form>
        </div>
    )
  }
}

CategoryAddForm.propTypes = {
  actions: PropTypes.object.isRequired
};

export default CategoryAddForm;
