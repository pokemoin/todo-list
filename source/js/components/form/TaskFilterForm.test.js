import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import TaskFilterForm from './TaskFilterForm';

// test('renders correctly', () => {
//   const tree = renderer.create(
//       <TaskFilterForm {...props} />
//   ).toJSON();
//   expect(tree).toMatchSnapshot();
// });


const setup = () => {

  const props = {
    actions: {
      taskFilterSubmit: jest.fn(),
      taskFilterReset: jest.fn()
    },
    options: {},
    filter: {
      isSearchActive: false,
      basicConfig: {
        showDone: false,
        taskTitle: ''
      },
      filterQuery: {
        showDone: false,
        taskTitle: ''
      }
    },
    activeTask: {},
    activeCategory: {},
    isEditTask: false,
  };

  const enzymeWrapper = shallow(<TaskFilterForm {...props} />);

  return {
    props,
    enzymeWrapper
  }
};

describe('components', () => {
  describe('TaskFilterForm', () => {

    it('should render self and subcomponents', () => {
      const { enzymeWrapper } = setup();

      expect(enzymeWrapper.find('form').hasClass('filter-form')).toBe(true);

      const showDoneInputProps = enzymeWrapper.find('[name="showDone"]').props();
      expect(showDoneInputProps.type).toEqual('checkbox');
      expect(showDoneInputProps.name).toEqual('showDone');

      const taskTitleInputProps = enzymeWrapper.find('[name="taskTitle"]').props();
      expect(taskTitleInputProps.type).toEqual('text');
      expect(taskTitleInputProps.name).toEqual('taskTitle');
      expect(taskTitleInputProps.placeholder).toEqual('Search');
    });
  })
});
