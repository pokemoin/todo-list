import React from 'react';
import PropTypes from 'prop-types';

class TaskAddForm extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleSubmit(e) {
    e.preventDefault();

    const {actions} = this.props;
    const form = e.target;
    const taskTitle = form.elements.taskTitle.value.trim();

    if (taskTitle !== '') {
      form.classList.remove('error');
      form.elements.taskTitle.value = '';
      actions.createNewTask(taskTitle);
    } else {
      form.classList.add('error');
    }
  }
  render() {
    return (
        <div className='task-add-form add-form-block'>
          <form className='form-add' onSubmit={this.handleSubmit}>
            <input type='text'
                   name='taskTitle'
                   placeholder='Enter task title'
            />
            <button>
              Add
            </button>
          </form>
        </div>
    )
  }
}

TaskAddForm.propTypes = {
  actions: PropTypes.object.isRequired
};

export default TaskAddForm;
