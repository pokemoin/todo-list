import React from 'react';
import PropTypes from 'prop-types';

class ModalCategoryEditForm extends React.Component {
  constructor (props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }
  handleSubmit(e) {
    e.preventDefault();

    const {actions} = this.props;
    const form = e.target;
    const categoryTitle = form.elements.categoryTitle.value.trim();

    if (categoryTitle !== '') {
      form.classList.remove('error');
      form.elements.categoryTitle.value = '';
      actions.submitEditCategoryModalForm(categoryTitle);
    } else {
      form.classList.add('error');
    }
  }
  handleClose(e) {

    const {actions} = this.props;
    let className = e.target.className;

    if (/modal$/.test(className) || /close/.test(className)) {
      actions.toggleShowEditCategoryForm(false);
    }
  }
  render () {

    return (
        <div className='modal'
             onClick={this.handleClose}
        >
          <div className="modal-content">
            <div className="close"></div>
            <h3 className="modal-title">Enter new category name</h3>
            <div className='category-add-form add-form-block'>
              <form className='form-add' onSubmit={this.handleSubmit}>
                <input type='text'
                       name='categoryTitle'
                       placeholder='Enter category title'
                />
                <button>
                  Add
                </button>
              </form>
            </div>
          </div>
        </div>
    )
  }
}

ModalCategoryEditForm.propTypes = {
  actions: PropTypes.object.isRequired
};

export default ModalCategoryEditForm;
