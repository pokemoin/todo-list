import React from 'react';
import PropTypes from 'prop-types';
import {NavLink, Redirect} from 'react-router-dom';

import CategoryList from '../list/CategoryList';

class CategoryListItem extends React.Component {
  constructor(props) {
    super(props);
    this.renderInnerList = this.renderInnerList.bind(this);
    this.handleClick = this.handleClick.bind(this);
  }
  handleClick(e) {

    const {category, actions, isEditTask} = this.props;
    const {isActive} = category;
    const targetClassList = e.target.classList;

    if (!isActive && !isEditTask) {
      actions.updateActiveCategory(category);
    }

    if (targetClassList.contains('category-edit')) {
      actions.toggleShowEditCategoryForm(true);
    } else if (targetClassList.contains('category-remove')) {
      actions.removeCategory(category);
    } else if (targetClassList.contains('add-sub-category')) {
      actions.toggleShowSubCategoryForm(true);
    } else if (targetClassList.contains('move-task')) {
      actions.moveEditTaskToCategory(category);
    }
  }
  renderInnerList() {

    const {category: {children}} = this.props;
    const props = {
      ...this.props,
      categoryList: children,
      listClassName: "sub-category-list"
    };

    return (
        <CategoryList {...props} />
    )
  }
  render() {

    const {category: {title, id, children, isActive}, isEditTask, ownProps: {match}} = this.props;
    const isChildren= Object.keys(children).length > 0;
    const categoryMultiClass = isChildren ? ' category-multi-item' : '';
    const categoryMoveHideClass = !isActive && isEditTask ? '' : ' hide';
    let path = `/${id}`;

    if (isEditTask) {
      path += `/${match.params.taskId}/${match.params.action}`;
    }

    return (
        <li className={`category${categoryMultiClass}`}>
          {isActive && <Redirect to={path} />}
          <NavLink to={path}
                   activeClassName='active'
                   onClick={this.handleClick}
          >
            {title}
            <div className="actions">
              <button className="action category-edit"></button>
              <button className="action category-remove"></button>
              <button className="action add-sub-category"></button>
              <button className={`action move-task${categoryMoveHideClass}`}></button>
            </div>
          </NavLink>
          {isChildren && this.renderInnerList()}
        </li>
    )
  }
}

CategoryListItem.propTypes = {
  actions: PropTypes.object.isRequired,
  category: PropTypes.object.isRequired,
  isEditTask: PropTypes.bool.isRequired,
  activeTask: PropTypes.object.isRequired,
  ownProps: PropTypes.object.isRequired
};

export default CategoryListItem;
