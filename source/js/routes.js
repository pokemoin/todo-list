import React from 'react';
import {Route, Switch} from 'react-router-dom';

import App from './containers/App';
import NotFound from './containers/NotFound';

export default <Switch>
  <Route path="/:categoryId?/:taskId?/:action?" component={App} />
  <Route component={NotFound}/>
</Switch>;

