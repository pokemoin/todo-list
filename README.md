### React JS, React Router 4, Redux, Redux-Saga, ES6, Webpack 2

install Node JS: https://nodejs.org/en/

* open console
* open project root folder
* install dependencies
```
npm i
```
*  run dev server
```
npm run dev
```

*  create bulld
```
npm run prod
```

* run test
```
npm test
```
